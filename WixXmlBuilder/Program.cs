﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;

using Mono.Options;


namespace WixXmlBuilder
{
    class Program
    {
        static void ShowHelp(OptionSet p)
        {
            Console.WriteLine("Usage: greet [OPTIONS]+ message");
            Console.WriteLine("Greet a list of individuals with an optional message.");
            Console.WriteLine("If no message is specified, a generic greeting is used.");
            Console.WriteLine();
            Console.WriteLine("Options:");
            p.WriteOptionDescriptions(Console.Out);
        }

        static void Main(string[] args)
        {
            bool showHelp = false;
            string productName = "[My Product Name]";
            string manufacturer = "[My Manfacturer]";
            string sourcePrefix = string.Empty;
            string directoryToLoad = string.Empty;
            string outputFile = @"Product.wxs";

            var p = new OptionSet()
            {
                {"n|productName=", "the name of the product", n=>productName=n},
                {"m|manufacturer=", "the name of the manufacturer", m=>manufacturer=m},
                {"s|sourcePrefix=", "The prefix to append to each source", s=>sourcePrefix=s},
                {"o|outfile=", "The file to output the wxs to", o=>outputFile=o},
                {"d|inputDirectory=", "The root directory to source all files from", d=>directoryToLoad=d},
                {"h|help",  "show this message and exit", v => showHelp = v != null }
            };

            try
            {
                p.Parse(args);
            }
            catch (Exception e)
            {
                Console.Write("WixXmlBuilder: ");
                Console.WriteLine(e.Message);
                Console.WriteLine("Try `WixXmlBuilder --help' for more information.");
                return;
            }

            if (showHelp)
            {
                ShowHelp(p);
                return;
            }

            if (string.IsNullOrEmpty(directoryToLoad))
            {
                Console.WriteLine("An input directory is needed");
                return;
            }

            DirectoryInfo di = new DirectoryInfo(directoryToLoad);
            if (!di.Exists)
            {
                Console.WriteLine("Directory {0} not found", directoryToLoad);
                return;
            }

            XmlDocument doc = new XmlDocument();

            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement docRoot = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, docRoot);

            XmlNode root = doc.CreateElement("Wix");
            XmlAttribute ns = doc.CreateAttribute("xmlns");
            ns.Value = "http://schemas.microsoft.com/wix/2006/wi";
            root.Attributes.Append(ns);
            doc.AppendChild(root);

            XmlElement productSection = GenerateProductSection(doc, productName, manufacturer);
            root.AppendChild(productSection);
            
            Dictionary<string, string> directoryToIds;
            XmlElement fragmentInstallSection = GenerateFragmentSection(doc, directoryToLoad, out directoryToIds);
            root.AppendChild(fragmentInstallSection);

            List<string> componentGroupIds;
            XmlElement fragmentFileListSection = GenerateFragmentFileListSection(doc, directoryToLoad, sourcePrefix, directoryToIds, out componentGroupIds);
            root.AppendChild(fragmentFileListSection);

            GenerateFeature(doc, productSection, componentGroupIds);

            doc.Save(outputFile);


        }

        private static void GenerateFeature(XmlDocument doc, XmlElement productSection, List<string> componentGroupIds)
        {
            XmlElement feature = doc.CreateElement("Feature");
            
            XmlAttribute idAtt = doc.CreateAttribute("Id");
            idAtt.Value = "ProductFeature";

            XmlAttribute titleAtt = doc.CreateAttribute("Title");
            titleAtt.Value = "Title";

            XmlAttribute levelAtt = doc.CreateAttribute("Level");
            levelAtt.Value = "1";

            feature.Attributes.Append(idAtt);
            feature.Attributes.Append(titleAtt);
            feature.Attributes.Append(levelAtt);

            foreach (string componentGroupId in componentGroupIds)
            {
                XmlElement componentGroupRef = doc.CreateElement("ComponentGroupRef");

                idAtt = doc.CreateAttribute("Id");
                idAtt.Value = componentGroupId;

                componentGroupRef.Attributes.Append(idAtt);

                feature.AppendChild(componentGroupRef);
            }

            productSection.AppendChild(feature);
        }

        private static XmlElement GenerateFragmentSection(XmlDocument doc, string directoryToLoad, out Dictionary<string, string> directoryToIds)
        {
            XmlElement fragment = doc.CreateElement("Fragment");
            directoryToIds = new Dictionary<string, string>();
            DirectoryInfo di = new DirectoryInfo(directoryToLoad);
            XmlElement dir = doc.CreateElement("Directory");
            XmlAttribute dirId = doc.CreateAttribute("Id");
            dirId.Value = "INSTALLFOLDER";
            XmlAttribute dirName = doc.CreateAttribute("Name");
            dirName.Value = di.Name;
            dir.Attributes.Append(dirId);
            dir.Attributes.Append(dirName);

            directoryToIds[di.FullName] = dirId.Value;
            foreach (var directory in di.EnumerateDirectories())
            {
                XmlElement toAdd = GenerateFragmentSectionChildren(doc, directory.FullName, directoryToIds);
                dir.AppendChild(toAdd);
            }
            fragment.AppendChild(dir);
            return fragment;
        }

        private static XmlElement GenerateFragmentSectionChildren(XmlDocument doc, string directoryToLoad, Dictionary<string, string> directoryToIds)
        {
            DirectoryInfo di = new DirectoryInfo(directoryToLoad);
            XmlElement dir = doc.CreateElement("Directory");
            XmlAttribute dirId = doc.CreateAttribute("Id");
            dirId.Value = string.Format("dir_{0}", Guid.NewGuid().ToString("N").ToUpper());
            XmlAttribute dirName = doc.CreateAttribute("Name");
            dirName.Value = di.Name;
            dir.Attributes.Append(dirId);
            dir.Attributes.Append(dirName);
            directoryToIds[di.FullName] = dirId.Value;
            foreach (var directory in di.EnumerateDirectories())
            {
                XmlElement toAdd = GenerateFragmentSectionChildren(doc, directory.FullName, directoryToIds);
                dir.AppendChild(toAdd);
            }

            return dir;
        }

        private static XmlElement GenerateFragmentFileListSection(XmlDocument doc, string rootDirectory, string sourcePrefix, Dictionary<string, string> directoryToIds, out List<string> componentGroupIds)
        {
            componentGroupIds = new List<string>();
            if (string.IsNullOrEmpty(sourcePrefix))
            {
                sourcePrefix = ".";
            }

            XmlElement fragment = doc.CreateElement("Fragment");

            string[] allFiles = Directory.GetFiles(rootDirectory, "*.*", SearchOption.AllDirectories);
            DirectoryInfo di = new DirectoryInfo(rootDirectory);

            Dictionary<string, XmlElement> componentGroups = new Dictionary<string, XmlElement>();

            XmlElement componentGroup = doc.CreateElement("ComponentGroup");

            XmlAttribute idAtt = doc.CreateAttribute("Id");
            idAtt.Value = "ProductComponents";

            XmlAttribute directoryAtt = doc.CreateAttribute("Directory");
            directoryAtt.Value = "INSTALLFOLDER";

            componentGroup.Attributes.Append(idAtt);
            componentGroup.Attributes.Append(directoryAtt);

            componentGroups[rootDirectory] = componentGroup;

            foreach (string file in allFiles)
            {
                string shortName = Path.GetFullPath(file).Replace(di.FullName, "");
                FileInfo fi = new FileInfo(file);
                DirectoryInfo thisDir = fi.Directory;
                if (!componentGroups.ContainsKey(thisDir.FullName))
                {
                    XmlElement componentGroupElem = doc.CreateElement("ComponentGroup");

                    idAtt = doc.CreateAttribute("Id");
                    string newComponentGroupId = Guid.NewGuid().ToString("N").ToUpper();
                    componentGroupIds.Add(newComponentGroupId);
                    idAtt.Value = newComponentGroupId;

                    directoryAtt = doc.CreateAttribute("Directory");
                    directoryAtt.Value = directoryToIds[thisDir.FullName];

                    componentGroupElem.Attributes.Append(idAtt);
                    componentGroupElem.Attributes.Append(directoryAtt);

                    componentGroups[thisDir.FullName] = componentGroupElem;
                }

                XmlElement group = componentGroups[thisDir.FullName];
                XmlElement cmp = GenerateComponent(doc, shortName, sourcePrefix);
                group.AppendChild(cmp);
            }



            foreach (var value in componentGroups.Values)
            {
                fragment.AppendChild(value);
            }

            return fragment;

        }

        private static XmlElement GenerateComponent(XmlDocument doc, string filename, string sourcePrefix)
        {
            string underName = Guid.NewGuid().ToString("N").ToUpper();//filename.Replace(".", "").Replace("\\", "").Replace("-", "_").Replace("+", "").Replace(" ", "");

            XmlElement cmp = doc.CreateElement("Component");

            XmlAttribute idAtt = doc.CreateAttribute("Id");
            idAtt.Value = string.Format("CMP_{0}", underName);

            XmlAttribute guidAtt = doc.CreateAttribute("Guid");
            guidAtt.Value = Guid.NewGuid().ToString("N").ToUpper();

            cmp.Attributes.Append(idAtt);
            cmp.Attributes.Append(guidAtt);

            XmlElement file = doc.CreateElement("File");
            idAtt = doc.CreateAttribute("Id");
            idAtt.Value = string.Format("FILE_{0}", underName);

            XmlAttribute sourceAtt = doc.CreateAttribute("Source");
            sourceAtt.Value = string.Format("{0}{1}", sourcePrefix, filename);

            XmlAttribute keyPathAtt = doc.CreateAttribute("KeyPath");
            keyPathAtt.Value = "yes";

            file.Attributes.Append(idAtt);
            file.Attributes.Append(sourceAtt);
            file.Attributes.Append(keyPathAtt);

            cmp.AppendChild(file);
            return cmp;
        }

        private static XmlElement GenerateProductSection(XmlDocument doc, string productName, string manufacturer)
        {
            XmlElement product = doc.CreateElement("Product");

            XmlAttribute idAtt = doc.CreateAttribute("Id");
            idAtt.Value = "*";

            XmlAttribute nameAtt = doc.CreateAttribute("Name");
            nameAtt.Value = productName;

            XmlAttribute languageAtt = doc.CreateAttribute("Language");
            languageAtt.Value = "1033";

            XmlAttribute versionAtt = doc.CreateAttribute("Version");
            versionAtt.Value = "$(var.BuildNumber)";

            XmlAttribute manufacturerAtt = doc.CreateAttribute("Manufactruer");
            manufacturerAtt.Value = manufacturer;

            XmlAttribute upgradeCodeAtt = doc.CreateAttribute("UpgradeCode");
            upgradeCodeAtt.Value = Guid.NewGuid().ToString("N").ToUpper();

            product.Attributes.Append(idAtt);
            product.Attributes.Append(nameAtt);
            product.Attributes.Append(languageAtt);
            product.Attributes.Append(versionAtt);
            product.Attributes.Append(manufacturerAtt);
            product.Attributes.Append(upgradeCodeAtt);

            #region GenPackage

            XmlElement package = doc.CreateElement("Package");

            XmlAttribute installerVersionAtt = doc.CreateAttribute("InstallerVersion");
            installerVersionAtt.Value = "200";

            XmlAttribute compressedAtt = doc.CreateAttribute("Compressed");
            compressedAtt.Value = "yes";

            XmlAttribute installScopeAtt = doc.CreateAttribute("InstallScope");
            installScopeAtt.Value = "perMachine";

            XmlAttribute descriptionAtt = doc.CreateAttribute("Description");
            descriptionAtt.Value = "[Insert your description]";

            XmlAttribute commentsAtt = doc.CreateAttribute("Comments");
            commentsAtt.Value = string.Format("(c) {0} {1}", manufacturer, DateTime.Today.Year);

            XmlAttribute platformAtt = doc.CreateAttribute("Platform");
            platformAtt.Value = "x64";

            package.Attributes.Append(installerVersionAtt);
            package.Attributes.Append(compressedAtt);
            package.Attributes.Append(installScopeAtt);
            package.Attributes.Append(manufacturerAtt);
            package.Attributes.Append(descriptionAtt);
            package.Attributes.Append(commentsAtt);
            package.Attributes.Append(platformAtt);

            product.AppendChild(package);

            #endregion


            #region MajorUpgrade

            XmlElement majorUpgrade = doc.CreateElement("MajorUpgrade");
            XmlAttribute downgradeErrorMessageAtt = doc.CreateAttribute("DowngradeErrorMessage");
            downgradeErrorMessageAtt.Value = "A new version of [ProductName] is already installed.";
            majorUpgrade.Attributes.Append(downgradeErrorMessageAtt);
            product.AppendChild(majorUpgrade);

            #endregion

            #region MediaTemplate

            XmlElement mediaTemplate = doc.CreateElement("MediaTemplate");
            XmlAttribute embedCabAtt = doc.CreateAttribute("EmbedCab");
            embedCabAtt.Value = "yes";
            mediaTemplate.Attributes.Append(embedCabAtt);
            product.AppendChild(mediaTemplate);

            #endregion


            return product;
        }
    }
}

